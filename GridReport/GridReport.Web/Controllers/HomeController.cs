﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using FastMember;
using GridReport.Data;
using GridReport.Web.Data;
using GridReport.Web.Models;


namespace GridReport.Web.Controllers
{
    public class HomeController : Controller
    {
        public static IEnumerable<Record> Data = new RandomData().CreateRandomData(100);
        public ActionResult Index(RecordModel model)
        {
            var data = Data;
            data = Search(data, model.filter);
            data = SearchData(data, model.from,model.to);
            model.Data = data;
            return View(model);
        }
        public IEnumerable<T> Search<T>(IEnumerable<T> data, string filter)
        {
            if (string.IsNullOrEmpty(filter))
                return data;
            var rowType = typeof (T);
            var accessor = TypeAccessor.Create(rowType);
            var names = (from prop in rowType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                         select prop.Name);
            return
                data.Where(row => names.Select(name => accessor[row, name].ToString()).Any(val => val.Contains(filter)));
        }
        // Фильтрует по всем датам
        public IEnumerable<T> SearchData<T>(IEnumerable<T> data, DateTime? from, DateTime? to)
        {
            if (!from.HasValue && !to.HasValue)
                return data;
            var rowType = typeof (T);
            var accessor = TypeAccessor.Create(rowType);
            var names = (from prop in rowType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                         select prop.Name);
            if (to.HasValue)
                data = data.Where(row => names.Select(name => accessor[row, name] as DateTime?).Any(val => val <= to));
            if (from.HasValue)
                data = data.Where(row => names.Select(name => accessor[row, name] as DateTime?).Any(val => val >= from));
            return data;
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
