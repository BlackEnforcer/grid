﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GridReport.Web.Data;

namespace GridReport.Web.Models
{
    public class RecordModel
    {
        public IEnumerable<Record> Data { get; set; }
        [Display(Name = "Указать период от")]
        public DateTime? from { get; set; }
        [Display(Name = "до")]
        public DateTime? to { get; set; }
        public string filter { get; set; }
    }
}
