﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace GridReport.Web.Data
{
        [DataContract]
        public class Record
        {
            /// <summary>
            /// Отчет показывается Продаву в московском времени, поэтому разбивку по дням тоже проще делать в МСК
            /// </summary>
            [DataMember(Order = 1)]
            public DateTime DateMsk { get;  set; }

            [DataMember(Order = 2)]
            public decimal StartDebt { get;  set; }

            [DataMember(Order = 3)]
            public decimal Income { get;  set; }

            [DataMember(Order = 4)]
            public decimal Returns { get;  set; }

            [DataMember(Order = 5)]
            public decimal Payoff { get;  set; }

            [DataMember(Order = 6)]
            public decimal Penalty { get;  set; }

            public decimal EndDebt
            {
                get { return StartDebt + Income - Returns - Payoff - Penalty; }
            }

            internal static Record NewDayRecord(DateTime newDayMsk, decimal startDebt)
            {
                return new Record { DateMsk = newDayMsk, StartDebt = startDebt };
            }

            /// <summary>
            /// Апдейтит данную запись отчета в соотв. с указанной проводкой и возвращает изменение Задолженности агента на конец дня с учетом данной проводки.
            /// </summary>


            public void ChangeStartDebt(decimal debtDelta)
            {
                StartDebt += debtDelta;
            }
        }
    }