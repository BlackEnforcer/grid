﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GridReport.Web.Data;

namespace GridReport.Data
{
    public class People
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName{ get; set; }
        public string City{ get; set; }
        public string Title{ get; set; }
        public DateTime BirthDate{ get; set; }
        public int Age{ get; set; }
    }

    public class RandomData
    {
        private string[] firstNames =
            {
                "Nancy", "Andrew", "Janet", "Margaret", "Steven", "Michael", "Robert", "Laura",
                "Anne", "Nige"
            };

        private string[] lastNames =
            {
                "Davolio", "Fuller", "Leverling", "Peacock", "Buchanan", "Suyama", "King",
                "Callahan", "Dodsworth", "White"
            };

        private string[] cities =
            {
                "Seattle", "Tacoma", "Kirkland", "Redmond", "London", "Philadelphia", "New York",
                "Seattle", "London", "Boston"
            };

        private string[] titles =
            {
                "Accountant", "Vice President, Sales", "Sales Representative", "Technical Support",
                "Sales Manager", "Web Designer", "Software Developer", "Inside Sales Coordinator",
                "Chief Techical Officer", "Chief Execute Officer"
            };

        private DateTime[] birthDates =
            {
                new DateTime(1948, 12, 08), new DateTime(1952, 02, 19),
                new DateTime(1963, 08, 30), new DateTime(1937, 09, 19),
                new DateTime(1955/03/04), new DateTime(1963/07/02), new DateTime(1960/05/29),
                new DateTime(1958/01/09), new DateTime(1966/01/27), new DateTime(1966/03/27)
            };

        private int Floor(decimal value)
        {
            return (int) Math.Floor(value);
        }

        public IEnumerable<Record> CreateRandomData(int count)
        {
            var data = new List<Record>();
            var random = new Random();
            for (var i = 0; i < count; i++)
            {
                var firstName = firstNames[Floor(random.Next(10))];
                var lastName = lastNames[Floor(random.Next(10))];
                var city = cities[Floor(random.Next(10))];
                var title = titles[Floor(random.Next(10))];
                var birthDate = birthDates[Floor(random.Next(10))];
                var age = DateTime.Now.Year - birthDate.Year;
                data.Add(new Record
                {
                    DateMsk = birthDate,
                    StartDebt = age,
                    Income = age * random.Next(100),
                    Payoff = age * random.Next(33),Penalty = age*random.Next(9),Returns = random.Next(1000)
                                 
                             });
            }
            return data;
        }
    }
}